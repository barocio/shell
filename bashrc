# -*- mode:shell-script -*-

source ~/.config/shell/aliases
source ~/.config/shell/environment

if [[ $- == *i* ]] ; then
    source ~/.config/shell/bash_prompt
    gpgconf --launch gpg-agent
fi
